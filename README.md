# Smartmupen #

## The simple way to sort your cheat codes on mupen64plus Linux ##]

Since mupen64plus decided to remove their GUI in some of their most recent updates for Linux, I created this little tool to store and update your cheat preferences. Place your ROM files in ```~/Documents/ROMs/``` and Smartmupen will scan and store your cheat preferences.

To compile, install ```fpc``` the Free Pascal Compiler or the Lazarus IDE. Load the project (```smartmupen.lpr```) in Lazarus and press F9, or run ```fpc smartmupen.lpr``` and place the generated binary somewhere in your path.

TODO: Add parsing of mupen64plus cheat database