unit unit1;

{$mode objfpc}{$H+}

interface

uses
	Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, StdCtrls, Unix;

type

	{ TMainWindow }

 TGame = record
   Name, Cheats: String[255];
 end;

 TDatabase = file of TGame;

 TMainWindow = class(TForm)
		RefreshButton: TButton;
		LaunchButton: TButton;
		CheatLabel: TLabel;
		CheatsList: TEdit;
		RomList: TListBox;
		RomLabel: TLabel;
  procedure FormCreate(Sender: TObject);
		procedure LaunchButtonClick(Sender: TObject);
		procedure RefreshButtonClick(Sender: TObject);
		procedure RomListSelectionChange(Sender: TObject; User: boolean);
	private
		Db: TDatabase;
    procedure SetDbFile;
		procedure FillRomList;
    function DbFile(): String;
    procedure DeleteRecord(RecordName: String);
    procedure AddRecord(NewRecord: TGame);
	public
		function GetRecord(RecordName: String): TGame;
	end;

var
	MainWindow: TMainWindow;

implementation

{$R *.lfm}

{ TMainWindow }

procedure TMainWindow.SetDbFile;
begin
  if not FileExists(DbFile()) then
    Rewrite(Db)
	else
    Reset(Db);
end;

procedure TMainWindow.DeleteRecord(RecordName: String);
var
  Games: array of TGame;
  TempFile: TGame;
  Index, Skipped: Integer;
begin
  SetLength(Games, FileSize(Db));
  Seek(Db, 0);

  Index:= 0;
  Skipped:= 0;

  while not EOF(Db) do
  begin
    Read(Db, TempFile);
    if TempFile.Name = RecordName then begin
      Index:= Index - 1;
      Skipped:= Skipped + 1
		end
    else begin
      Games[Index]:= TempFile;
		end;
    Index:= Index + 1;
	end;

  SetLength(Games, FileSize(Db) - Skipped);
  Rewrite(Db);
  Seek(Db, 0);

  for TempFile in Games do
  begin
    Write(Db, TempFile)
	end
end;

procedure TMainWindow.AddRecord(NewRecord: TGame);
begin
  Seek(Db, FileSize(Db));
  Write(Db, NewRecord);
end;

function TMainWindow.DbFile(): String;
begin
  DbFile:= GetUserDir + '/.config/smartmupen.dat';
end;

function TMainWindow.GetRecord(RecordName: String): TGame;
var
  EmptyGame: TGame;
begin
  EmptyGame.Name:= RecordName;
  EmptyGame.Cheats:= '';
  Seek(Db, 0);
  while not EOF(Db) do
	begin
    Read(Db, GetRecord);
    if GetRecord.Name = RecordName then
    	break
    else
      GetRecord:= EmptyGame;
	end;
end;

procedure TMainWindow.FillRomList;
var
  RomFiles: TStringList;
begin
  ChDir(GetUserDir + '/Documents/ROMs');
  RomFiles:= FindAllFiles('', '*.z64', false);
  RomFiles.Sort;
  RomList.Items.Assign(RomFiles);
end;

procedure TMainWindow.FormCreate(Sender: TObject);
begin
  FillRomList;
  AssignFile(Db, DbFile());
  SetDbFile;
end;

procedure TMainWindow.LaunchButtonClick(Sender: TObject);
var
  Game: TGame;
begin
  Game:= GetRecord(RomList.GetSelectedText);
  Game.Cheats:= CheatsList.Text;
  DeleteRecord(Game.Name);
  AddRecord(Game);
  if CheatsList.Text = '' then begin
    Visible:= False;
    SysUtils.ExecuteProcess('/usr/games/mupen64plus', ' "' + Game.Name + '"');
    Visible:= True
	end
	else begin
    MainWindow.Visible:= False;
    SysUtils.ExecuteProcess('/usr/games/mupen64plus', ' --cheats ' + Game.Cheats + ' "' + Game.Name + '"');
    MainWindow.Visible:= True;
	end;
end;

procedure TMainWindow.RefreshButtonClick(Sender: TObject);
begin
  FillRomList;
end;

procedure TMainWindow.RomListSelectionChange(Sender: TObject; User: boolean);
var
  Game: TGame;
begin
  Game:= GetRecord(RomList.GetSelectedText);
  CheatsList.Text:= UTF8ToAnsi(Game.Cheats);
end;

end.

